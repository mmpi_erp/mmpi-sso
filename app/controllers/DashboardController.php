<?php

class DashboardController extends ControllerBase
{
    protected function initialize()
    {
        if (!$this->session->has('username')) {
                // assuming that auth param is initialized after login
                return $this->response->redirect(BASE_URI);
				//COMMENT
                // then redirect to your login page
        }   
    }
    public function indexAction()
    {
        $this->view->setVar('page_active', 'dashboard');
    	$this->view->setVar('page_content', 'dashboard/index');
    }
    
}

