<?php

class IndexController extends ControllerBase
{
    protected function initialize()
    {//TEST
        if ($this->session->has('username')) {
           
            return $this->response->redirect(BASE_URI."dashboard");
        }
        
    }
    public function indexAction()
    {
        $this->view->setVar('error_prompt','');
    	$this->view->setVar('page_content', 'index/index');
    }
    
    public function loginAction()
    {
        
        $page_content = "";
        $error_prompt = "";
        if($this->request->isPost())
        {
            $username = $this->request->getPost('username');
            $password = $this->request->getPost('password');
            //$ipaddress = $this->request->getPost('ipaddress');
            $ipaddress = ' ';
            
            //$user_info = UserInfoView::findFirst("user_id = '$username' and password = '$password'");
            $ldap_authenticate = 1;
            //$ldap_authenticate = $this->tools->authenticateLDAP($username,$password);
            
            if($ldap_authenticate == 1)
            {
                //$user_info = UserInfoView::findFirst("user_id = '$username'");
                $user_info = UserInfoView::findFirst("user_id = '$username' and password = '$password'");
                if ($user_info) 
                {
                    $page_content = "dashboard/index";

                    $system_access_list = SysAccessMatrix::getSystemAccess($user_info->job_id);
                    $sessionLogger = $this->setSessionVariables($username,$system_access_list,$user_info,$ipaddress);
                    $this->sessionLogger->logSession($sessionLogger);
                } 
                else
                {
                    $error_prompt = "User not defined in ERP";
                    $page_content = "index/index";
                }                
            }
            else if($ldap_authenticate == 2)
            {
                $error_prompt = "Cannot connect to LDAP";
                $page_content = "index/index";                
            }
            else if($ldap_authenticate == 0)
            {
                $error_prompt = "Invalid User Credential";
                $page_content = "index/index";              
            }
                       
 
            
            //AUDIT_LOGS
        }
        else
        {
            $page_content = 'index/index';
        }
        
        $this->view->setVar('error_prompt',$error_prompt);
        $this->view->setVar('page_content', $page_content);
    }
    
    
    public function setSessionVariables($username,$system_access_list,$user_info,$ipaddress)
    {
        /***SET MODEL for session_tb LOGGING***/
        $sessionLogger = new SessionInfo();
        $sessionLogger->username = $username;
        $sessionLogger->ip_address = $this->tools->getMAC();
        $sessionLogger->session_key = $this->session->getId();  
        
        /***SET SESSION VARIABLE***/
        $this->session->set('username',$username);
        $this->session->set('ip_address',$ipaddress);
        $this->session->set('system_access_list',$system_access_list);
        $this->session->set('job_id',$user_info->job_id);
        $this->session->set('id_number',$user_info->id_number);
        $this->session->set('department_id',$user_info->department_id);
        $this->session->set('fname',$user_info->fname);
        $this->session->set('lname',$user_info->lname);
        $this->session->set('mname',$user_info->mname);
        $this->session->set('email_office',$user_info->email_office);
        
        $this->session->set('company_id',$user_info->company_id);
        $this->session->set('company_id_session',$user_info->company_id);
        $this->session->set('branch_id',$user_info->branch_id);
        $this->session->set('company_name',$user_info->company_name);
        $this->session->set('employee_stat',$user_info->employee_stat);
        $this->session->set('job_title',$user_info->job_title);
        
        if($this->session->get('branch_id') == '')
        {
            $this->session->set('department_name',$user_info->department_name);

        }
        else
        {
            $this->session->set('department_name',$user_info->branch_name);
            $this->session->set('department_id','99');
        }
        
        
        if ($user_info->job_id == '19') {
            $branch_ids = BranchInfo::getBranchesOM($user_info->id_number);
            $str='';
            foreach($branch_ids as $val)
            {
                $str = $str."'".$val->branch_id."'".",";
            }
            if($str=='') $str="''";
            $this->session->set('branch_ids',  rtrim($str,","));
        }        
        if ($user_info->job_id == '20') {
            $str='';
             $branch_ids = BranchInfo::getBranchesAM($user_info->id_number);
            foreach($branch_ids as $val)
            {
                $str = $str."'".$val->branch_id."'".",";
            }
            if($str=='') $str="''";
            $this->session->set('branch_ids',  rtrim($str,","));
        }
        else if ($user_info->job_id == '21') {
            $branch_ids = BranchInfo::getBranchesRM($user_info->id_number);
            $str='';
            foreach($branch_ids as $val)
            {
                $str = $str."'".$val->branch_id."'".",";
            }
            if($str=='') $str="''";
            $this->session->set('branch_ids',  rtrim($str,","));
        }
        
        if($user_info->department_id == '100' || $user_info->department_id == '10'|| $user_info->department_id == '60')
        {
            $branch_ids = BranchInfo::getBranchesEO($user_info->id_number);
            $str='';
            foreach($branch_ids as $val)
            {
                $str = $str."'".$val->branch_id."'".",";
            }
            if($str=='') $str="''";
            $this->session->set('branch_ids',  rtrim($str,","));   
        }

        return $sessionLogger;
    }
    public function logoutAction(){
        $this->session->destroy();
        return $this->response->redirect(BASE_URI);
    }    
}

