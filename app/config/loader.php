<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    array(
        $config->application->controllersDir,
        $config->application->modelsDir
    )
);
$loader->registerClasses(
    array(
        "SessionLogger" => "../app/helper/SessionLogger.php",
        "Tools" => "../app/helper/Tools.php"
    )
);
$loader->register();
