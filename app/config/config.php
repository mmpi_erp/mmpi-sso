<?php

defined('APP_PATH') || define('APP_PATH', realpath('.'));

$env = 'default';
$localIP = gethostbyname(trim(exec("hostname")));
$hostname = $_SERVER['HTTP_HOST'];
if (@$_SERVER['HTTP_HOST'] == 'localhost') {
    $env = 'localhost';
} else {
    if ($hostname == '107.6.113.222:8807') {
        $env = 'sg';
    } else if ($hostname == '195.100.100.17') {
        $env = 'sandbox';
    } else {
        $env = 'devops';
    }
}

//add base uri environment here
$baseuri = array();
$baseuri['localhost'] = 'http://localhost/MMPI-SSO/';
$baseuri['devops'] = 'http://sso.mmpi.ph/';
$baseuri['sandbox'] = 'http://195.100.100.17/mmpi-sso/';
$baseuri['sg'] = 'http://107.6.113.222:8807/mmpi-sso/';

$database['localhost'] = array(
    'adapter' => 'Mysql',
    'host' => '127.0.0.1',
    'username' => 'root',
    'password' => '',
    'dbname' => 'mmpi_hris',
    'charset' => 'utf8',
);

$database['devops'] = array(
    'adapter' => 'Mysql',
    'host' => '127.0.0.1',
    'username' => 'root',
    'password' => 'redhorsebeer',
    'dbname' => 'mmpi_hris',
    'charset' => 'utf8',
    'port' => 'port'
);
$database['sandbox'] = array(
    'adapter' => 'Mysql',
    'host' => '127.0.0.1',
    'username' => 'root',
    'password' => 'redhorsebeer',
    'dbname' => 'mmpi_hris',
    'charset' => 'utf8',
    'port' => 'port'
);

$database['sg'] = array(
    'adapter' => 'Mysql',
    'host' => '107.6.113.222',
    'username' => 'aws',
    'password' => 'aws1234',
    'dbname' => 'mmpi_hris',
    'charset' => 'utf8',
    'port' => '6033'
);
//set constant for base uri and image uri
define("BASE_URI", $baseuri[$env]);
define("LDAP_SERVER", "zentyal.mitsukoshimotors.com");
define("LDAP_PORT", "390");
define("LDAP_DC", "uid=USERNAME,ou=Users,dc=mmpimotors,dc=com");

return new \Phalcon\Config(array(
    'database' => $database[$env],
    'application' => array(
        'controllersDir' => APP_PATH . '/app/controllers/',
        'modelsDir' => APP_PATH . '/app/models/',
        'migrationsDir' => APP_PATH . '/app/migrations/',
        'viewsDir' => APP_PATH . '/app/views/',
        'pluginsDir' => APP_PATH . '/app/plugins/',
        'libraryDir' => APP_PATH . '/app/library/',
        'cacheDir' => APP_PATH . '/app/cache/',
        'baseUri' => $baseuri[$env],
    )
));

