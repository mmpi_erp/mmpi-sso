<?php

use Phalcon\Mvc\Model as PhModel,
    Phalcon\Mvc\Model\Query as PhQuery,
 	Phalcon\Mvc\Model\Message as PhMessage,
    Phalcon\Mvc\Model\Validator\Email as PhEmailValidator,
    Phalcon\Mvc\Model\Validator\Uniqueness as PhUniqueness,
	Phalcon\Db\RawValue as PhRawValue,
	Phalcon\Mvc\Url as PhUrl;

class SysAccessMatrix extends ModelBase
{
    public function initialize()
    {
	$this->keepSnapshots(true);$this->setSource('common_sys_access_matrix_tb');	
    }
    
    public function getSystemAccess($job_id) 
    {
        $phql = "SELECT 
                        sys_access.sys_access_matrix_id, sys_access.job_id, sys_access.is_accessible,
                        sys_info.system_name, sys_info.system_desc, sys_info.system_icon_location, sys_info.system_url, sys_info.system_href_id
                    FROM 
                        SysAccessMatrix sys_access
                    LEFT JOIN 
                        SystemInfo sys_info ON sys_access.system_id = sys_info.system_id 
                    WHERE 
                        sys_access.job_id = ?1 and 
                        sys_info.is_enabled = 1 order by sys_info.sequence";

        $data = $this->modelsManager->executeQuery($phql,array(1=>$job_id));
        return $data;
    }

}
