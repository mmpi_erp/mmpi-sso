<?php

use Phalcon\Mvc\Model as PhModel,
    Phalcon\Mvc\Model\Query as PhQuery,
 	Phalcon\Mvc\Model\Message as PhMessage,
    Phalcon\Mvc\Model\Validator\Email as PhEmailValidator,
    Phalcon\Mvc\Model\Validator\Uniqueness as PhUniqueness,
	Phalcon\Db\RawValue as PhRawValue,
	Phalcon\Mvc\Url as PhUrl;
use Phalcon\Mvc\Model\Resultset\Simple as Resultset;
class BranchInfo extends ModelBase
{
	public function initialize()
	{
		$this->keepSnapshots(true);$this->setSource('common_branch_info_tb');	
	}
        
	public function getBranchesAM($id_number){
		$sql = "select branch_id from common_branch_info_tb where am_id_number = '$id_number'";
		$pricelist = new BranchInfo();
        return new Resultset(null, $pricelist, $pricelist->getReadConnection()->query($sql));
	}   
	public function getBranchesRM($id_number){
		$sql = "select branch_id  from common_branch_info_tb where rm_id_number = '$id_number'";
		$pricelist = new BranchInfo();
        return new Resultset(null, $pricelist, $pricelist->getReadConnection()->query($sql));
	}      
	public function getBranchesOM($id_number){
		$sql = "select branch_id from common_branch_info_tb where om_id_number = '$id_number'";
		$pricelist = new BranchInfo();
        return new Resultset(null, $pricelist, $pricelist->getReadConnection()->query($sql));
	}    
        
	public function getBranchesEO(){
		$sql = "select branch_id from common_branch_info_tb";
		$pricelist = new BranchInfo();
        return new Resultset(null, $pricelist, $pricelist->getReadConnection()->query($sql));
	}            
}