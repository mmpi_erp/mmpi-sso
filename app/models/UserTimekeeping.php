<?php

use Phalcon\Mvc\Model as PhModel,
    Phalcon\Mvc\Model\Query as PhQuery,
 	Phalcon\Mvc\Model\Message as PhMessage,
    Phalcon\Mvc\Model\Validator\Email as PhEmailValidator,
    Phalcon\Mvc\Model\Validator\Uniqueness as PhUniqueness,
	Phalcon\Db\RawValue as PhRawValue,
	Phalcon\Mvc\Url as PhUrl;

class UserTimekeeping extends ModelBase
{
	public function initialize()
	{
		$this->keepSnapshots(true);$this->setSource('hris_user_timekeeping_tb');	
	}
	
	public function getBranchUsers($datenow='', $branch_user_id_array = '')
	{
		$user_id_text = "";
		if ($branch_user_id_array)		
		{
			for ($i=0; $i<count($branch_user_id_array); $i++)
			{
				if ($i==0) $user_id_text .= "(";
				if ($i != (count($branch_user_id_array)-1))
				{
					$user_id_text .= "'".$branch_user_id_array[$i]."',";
				}
				else $user_id_text .= "'".$branch_user_id_array[$i]."')";
			}
		}
		
		$phql = "SELECT * FROM UserTimekeeping utt
				 WHERE utt.date = '$datenow' AND utt.user_name IN $user_id_text";
		$data = $this->modelsManager->executeQuery($phql);
		return $data;
	}
	
	public function beforeValidation()
	{
		$this->date_updated = date('Y-m-d H:i:s');
		if (!@$this->logout_time) $this->logout_time = new PhRawValue('default');
		if (!@$this->attendance_count) $this->attendance_count = new PhRawValue('default');
		if (!@$this->day_type) $this->day_type = new PhRawValue('default');
		if (!@$this->status_flag) $this->status_flag = new PhRawValue('default');
		if (!@$this->date_created) $this->date_created = date('Y-m-d H:i:s');
	}
}